import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {
    public static int clientID = 0;
    private static ArrayList<ClientHandler> clientHandlers = new ArrayList<>();
    private static ExecutorService pool = Executors.newFixedThreadPool(10000);
    public static void main(String[] a) throws IOException {

        ServerSocket s = new ServerSocket(3000);
        System.out.println("Server is running");
        System.out.println("Waiting for clients...");

        while (true) {
            Socket socket = s.accept();
            clientID++;
            System.out.println("Client " + clientID + " is connected");
            ClientHandler clientThread = new ClientHandler(socket, clientID);
            clientHandlers.add(clientThread);
            pool.execute(clientThread);
        }
    }
}