import db.Handler;
import java.io.*;
import java.net.Socket;
import java.sql.SQLException;
import java.util.ArrayList;

public class ClientHandler implements Runnable{
    Handler db = new Handler();
    private Socket client;
    private BufferedReader reader;
    private PrintWriter writer;
    String tmp;
    String date;
    int clientID;
    public  ClientHandler(Socket clientSocket, int Id) throws IOException {
        this.client = clientSocket;
        this.clientID = Id;
        reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
        writer = new PrintWriter( client.getOutputStream(), true);
    }

    @Override
    public void run(){
        try {
            while (true) {
                String scene = null;
                try {
                    scene = reader.readLine();
                } catch (IOException e) {}
                try {
                    if (scene.equals("authorize")){
                        String login = null;
                        try {
                            login = String.valueOf(reader.readLine());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        String pass = String.valueOf(reader.readLine());

                        try {
                            writer.println(db.authorize(login, pass));
                        } catch (SQLException e) {
                            System.out.println("MySQL exception");
                        } catch (ClassNotFoundException e) {
                            System.out.println("There is no such class");
                        }




                    }else if (scene.equals("addData")){
                        String ln = String.valueOf(reader.readLine());
                        String fn = String.valueOf(reader.readLine());
                        String mn = String.valueOf(reader.readLine());
                        String gt = String.valueOf(reader.readLine());
                        String yt = String.valueOf(reader.readLine());
                        String tt = String.valueOf(reader.readLine());
                        String st = String.valueOf(reader.readLine());
                        try {
                            writer.println(db.addData(ln,fn,mn,gt,yt,tt,st));
                        } catch (SQLException e) {
                            System.out.println("MySQL exception");
                        } catch (ClassNotFoundException e) {
                            System.out.println("There is no such class");
                        }
                    }





                }catch (NullPointerException e){}
            }
        } catch (Exception e) {}
        finally {
            writer.close();
            try {
                reader.close();
            } catch (IOException e) {
                System.out.println("Client " + clientID + " has gone!");
            }
        }
    }
}
