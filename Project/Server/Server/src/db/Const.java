package db;

public class Const {
    public static final String ADMIN_TABLE = "successmonitoring.admins";
    public static final String ADMINS_LOGIN = "login";
    public static final String ADMIN_PASS = "password";

    public static final String AVERAGESCORE_TABLE = "successmonitoring.averagescore";
    public static final String AVERAGESCORE_ID = "idstudents";
    public static final String AVERAGESCORE_SCORE = "averagescore";
    public static final String AVERAGESCORE_YEAR = "year";
    public static final String AVERAGESCORE_TERM = "term";

    public static final String STUDENT_TABLE = "successmonitoring.students";
    public static final String STUDENTS_ID = "idstudents";
    public static final String STUDENTS_LASTNAME = "lastname";
    public static final String STUDENTS_FIRSTNAME = "firstname";
    public static final String STUDENTS_MIDDLENAME = "middlename";
    public static final String STUDENTS_GROUP = "group";
}