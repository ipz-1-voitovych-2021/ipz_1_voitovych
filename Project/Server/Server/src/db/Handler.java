package db;

import java.sql.*;
import java.util.ArrayList;
import java.util.Objects;

public class Handler extends Configs {
    Connection dbconnect;
    public Connection getDbConnect()
            throws ClassNotFoundException, SQLException{
        String connect = "jdbc:mysql://" + dbHost + ":"
                + dbPort + "/" + dbName;
        Class.forName("com.mysql.jdbc.Driver");
        dbconnect = DriverManager.getConnection(connect, dbUser, dbPass);
        return dbconnect;
    }

    public String addStudent(String lastName, String firstName, String middleName, String group)
            throws SQLException, ClassNotFoundException {
        Statement statement = getDbConnect().createStatement();
        ResultSet student =  statement.executeQuery(
                "SELECT lastname,firstname FROM successmonitoring.students WHERE lastname = '"
                        + lastName + "' AND firstname = '" + firstName + "'");
        String answer;
        if (student != null){
            answer = "alreadyExist";
        }else if(student==null){
            String insert = "INSERT INTO successmonitoring.students(lastname, firstname, middlename, group) VALUES(?,?,?,?)";
            try {
                PreparedStatement prSt = getDbConnect().prepareStatement(insert);
                prSt.setString(1, lastName);
                prSt.setString(2, firstName);
                prSt.setString(3, middleName);
                prSt.setString(4, group);
                prSt.executeUpdate();
            } catch (SQLException | ClassNotFoundException e) {
                e.printStackTrace();
            }
            answer = "added";
        }
        else {
            answer="error";
        }
        return answer;
    }

    public int getStudentId(String lastName, String firstName) throws SQLException, ClassNotFoundException {
        Statement statement = getDbConnect().createStatement();
        ResultSet student =  statement.executeQuery("SELECT idstudents, lastname, firstname FROM " +
                "successmonitoring.students WHERE lastname = '" + lastName + "' AND firstname = '" + firstName + "'");
        int studentId;
        if (student == null){
            ResultSet newStudent = statement.executeQuery("SELECT COUNT(*) FROM successmonitoring.students");
            studentId = newStudent.getInt("COUNT(*)");
            studentId ++;
            return studentId;
        }else {
            studentId = student.getInt(Const.STUDENTS_ID);
            return studentId;
        }
    }

    public String addData(String lastName, String firstName, String middleName, String group, String year,
                        String term, String score) throws SQLException, ClassNotFoundException {
        String student = addStudent(lastName, firstName, middleName, group);
        String answer;
        if (student!="error") {
            int studentId = getStudentId(lastName, firstName);
            Statement statement = getDbConnect().createStatement();
            ResultSet check = statement.executeQuery("SELECT idstudents, term FROM successmonitoring.everagescore WHERE" +
                    "idstudents = '" + studentId + "' AND term = '" + term + "'");
            if (check == null) {
                String insert = "INSERT INTO successmonitoring.averagescore(idstudents, everagescore, year, term) VALUES(?,?,?,?)";
                try {
                    PreparedStatement prSt = getDbConnect().prepareStatement(insert);
                    prSt.setInt(1, studentId);
                    prSt.setString(2, score);
                    prSt.setString(3, year);
                    prSt.setString(4, term);
                    prSt.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                answer = "added";
            } else {
                answer = "alreadyExist";
            }
            return answer;
        }else {
        answer="error";}
        return answer;
    }

// + метод для редагування: перевірка даних, що не співпадають, видалення попередніх даних(тільки ті,
//      що не співпадають), запис на їх місце нових даних
    public String edit(String lastName, String firstName, String middleName, String group, String year,
                       String term, String score) throws SQLException, ClassNotFoundException {
        String exist = addStudent(lastName, firstName, middleName, group);
        String answer;
        if (exist=="alreadyExist"){
            //      ???

            delete(lastName, firstName, year, term, score);
            addData(lastName, firstName, middleName, group, year, term, score);
            answer = "edited";
        }else {
            addData(lastName, firstName, middleName, group, year, term, score);
            answer="added";
        }
        return answer;
    }

    //ResultSet executeQuery only select

//DONE + метод для видалення даних: якщо позаданому студенту тільки 1 запис у табл з балами видалити все,
//      в інакшому випадку видалити тільки дані з табл з балами
    public void delete(String lastName, String firstName, String year, String term, String score)
            throws SQLException, ClassNotFoundException {
        int count=0, id = getStudentId(lastName, firstName);
        Statement statement = getDbConnect().createStatement();
        ResultSet check =  statement.executeQuery("SELECT" + Const.AVERAGESCORE_ID + "FROM"
                + Const.AVERAGESCORE_TABLE + "WHERE" + Const.AVERAGESCORE_ID + "=" + id);
        while (check.next()) {
            count++;
        }
        if (count > 1) {
            String delete = "DELETE FROM" + Const.AVERAGESCORE_TABLE + "WHERE" + Const.AVERAGESCORE_YEAR + "=?"
                    + "AND" + Const.AVERAGESCORE_TERM + "=?" + "AND" + Const.AVERAGESCORE_SCORE + "=?";
            PreparedStatement prSt = getDbConnect().prepareStatement(delete);
            prSt.setString(1, year);
            prSt.setString(2,term);
            prSt.setString(3,score);
            prSt.executeUpdate();
        }else {
            String delete = "DELETE FROM" + Const.STUDENT_TABLE + "WHERE" + Const.STUDENTS_LASTNAME + "=?"
                    + "AND" + Const.STUDENTS_FIRSTNAME + "=?";
            PreparedStatement prSt = getDbConnect().prepareStatement(delete);
            prSt.setString(1, lastName);
            prSt.setString(2, firstName);
            prSt.executeUpdate();
            String delete2 = "DELETE FROM" + Const.AVERAGESCORE_TABLE + "WHERE" + Const.AVERAGESCORE_ID + "= ?";
            PreparedStatement prSt2 = getDbConnect().prepareStatement(delete2);
            prSt2.setInt(1, id);
            prSt2.executeUpdate();
        }
    }

    public String authorize(String name, String password) throws SQLException, ClassNotFoundException {
        Statement statement = getDbConnect().createStatement();
        ResultSet admin =  statement.executeQuery("SELECT login, password FROM successmonitoring.admins");
        while (admin.next()) {
            String login;
            String pass;
            login = admin.getString(Const.ADMINS_LOGIN);
            pass = admin.getString(Const.ADMIN_PASS);
            if (login.equals(name) && pass.equals(password)) {
                return "admin";
            }
        }
        return "notAdmin";
    }

//DONE відображення рейтингу: якщо у групі - вивід рейтингу групи, серед всіх - вивід загального рейтингу
    public ArrayList<String> ratingView(String searchParam, String group)
            throws SQLException, ClassNotFoundException {
        ArrayList<String> list = new ArrayList<>();
        Statement statement = getDbConnect().createStatement();
        int studentId;
        if (Objects.equals(searchParam, "серед всіх")){
            ResultSet all = statement.executeQuery("SELECT" + Const.STUDENTS_ID + "," + Const.STUDENTS_LASTNAME
                    + "," + Const.STUDENTS_FIRSTNAME + "," + Const.STUDENTS_GROUP + "FROM" + Const.STUDENT_TABLE);
            if(all == null){
                return null;
            }
            while (all.next()) {
                studentId = getStudentId(Const.STUDENTS_LASTNAME, Const.STUDENTS_FIRSTNAME);
                ResultSet score = statement.executeQuery("SELECT" + Const.AVERAGESCORE_SCORE + "FROM"
                        + Const.AVERAGESCORE_TABLE + "WHERE" + Const.AVERAGESCORE_ID + "=" + studentId
                        + "AND" + Const.AVERAGESCORE_YEAR + "= 2020-2021");
                while (score.next()) {
                    list.add(all.getString(Const.STUDENTS_LASTNAME));
                    list.add(all.getString(Const.STUDENTS_FIRSTNAME));
                    list.add(all.getString(Const.STUDENTS_GROUP));
                    list.add(score.getString(Const.AVERAGESCORE_SCORE));
                }
            }
        }else {
            ResultSet inGroup = statement.executeQuery("SELECT" + Const.STUDENTS_ID + "," +
                    Const.STUDENTS_LASTNAME + "," + Const.STUDENTS_FIRSTNAME + "," + Const.STUDENTS_GROUP
                    + "FROM" + Const.STUDENT_TABLE + "WHERE" + Const.STUDENTS_GROUP + "=" + group);
            if(inGroup == null){
                return null;
            }
            while (inGroup.next()) {
                studentId = getStudentId(Const.STUDENTS_LASTNAME, Const.STUDENTS_FIRSTNAME);
                ResultSet score = statement.executeQuery("SELECT" + Const.AVERAGESCORE_SCORE + "FROM"
                        + Const.AVERAGESCORE_TABLE + "WHERE" + Const.AVERAGESCORE_ID + "=" + studentId + "AND"
                        + Const.AVERAGESCORE_YEAR + "= 2020-2021");
                while (score.next()) {
                    list.add(inGroup.getString(Const.STUDENTS_LASTNAME));
                    list.add(inGroup.getString(Const.STUDENTS_FIRSTNAME));
                    list.add(inGroup.getString(Const.STUDENTS_GROUP));
                    list.add(score.getString(Const.AVERAGESCORE_SCORE));
                }
            }
        }
        return list;
    }

//DONE відслідковування успішності: відобразити всі дані(бал, рік, семемтр) по заданому студенту
    public ArrayList<String> successMonitoring(String lastName, String firstName)
            throws SQLException, ClassNotFoundException {
        ArrayList<String> list = new ArrayList<>();
        int id = getStudentId(lastName, firstName);
        Statement statement = getDbConnect().createStatement();
        ResultSet data = statement.executeQuery("SELECT" + Const.AVERAGESCORE_YEAR + ","
                + Const.AVERAGESCORE_TERM + "," + Const.AVERAGESCORE_SCORE + "FROM"
                + Const.AVERAGESCORE_TABLE + "WHERE" + Const.AVERAGESCORE_ID + "=" + id);
        while (data.next()) {
            list.add(data.getString(Const.AVERAGESCORE_YEAR));
            list.add(data.getString(Const.AVERAGESCORE_TERM));
            list.add(data.getString(Const.AVERAGESCORE_SCORE));
        }
        return list;
    }

//DONE пошук найкращих: якщо у групі - вивід 10 кращих, серед всіх - 20
    public ArrayList<String> searchBest(String searchParam, String group)
            throws SQLException, ClassNotFoundException {
        ArrayList<String> list = new ArrayList<>();
        Statement statement = getDbConnect().createStatement();
        int studentId;
        if (Objects.equals(searchParam, "серед всіх")){
            ResultSet all = statement.executeQuery("SELECT" + Const.STUDENTS_ID + "," + Const.STUDENTS_LASTNAME
                    + "," + Const.STUDENTS_FIRSTNAME + "," + Const.STUDENTS_GROUP + "FROM" + Const.STUDENT_TABLE);
            if(all == null){
                return null;
            }
            while (all.next()) {
                studentId = getStudentId(Const.STUDENTS_LASTNAME, Const.STUDENTS_FIRSTNAME);
                ResultSet score = statement.executeQuery("SELECT" + Const.AVERAGESCORE_SCORE + "FROM"
                        + Const.AVERAGESCORE_TABLE + "WHERE" + Const.AVERAGESCORE_ID + "=" + studentId + "AND"
                        + Const.AVERAGESCORE_YEAR + "= '2020-2021' ORDERED BY" + Const.AVERAGESCORE_SCORE + "DESC");
                for(int i =0; i<20; i++) {
                    score.next();
                    list.add(all.getString(Const.STUDENTS_LASTNAME));
                    list.add(all.getString(Const.STUDENTS_FIRSTNAME));
                    list.add(all.getString(Const.STUDENTS_GROUP));
                    list.add(score.getString(Const.AVERAGESCORE_SCORE));
                }
            }
        }else {
            ResultSet inGroup = statement.executeQuery("SELECT" + Const.STUDENTS_ID + "," +
                    Const.STUDENTS_LASTNAME + "," + Const.STUDENTS_FIRSTNAME + "," + Const.STUDENTS_GROUP
                    + "FROM" + Const.STUDENT_TABLE + "WHERE" + Const.STUDENTS_GROUP + "=" + group);
            if(inGroup == null){
                return null;
            }
            while (inGroup.next()) {
                studentId = getStudentId(Const.STUDENTS_LASTNAME, Const.STUDENTS_FIRSTNAME);
                ResultSet score = statement.executeQuery("SELECT" + Const.AVERAGESCORE_SCORE + "FROM"
                        + Const.AVERAGESCORE_TABLE + "WHERE" + Const.AVERAGESCORE_ID + "=" + studentId + "AND"
                        + Const.AVERAGESCORE_YEAR + "= 2020-2021 ORDERED BY" + Const.AVERAGESCORE_SCORE + "DESC");
                for(int i =0; i<10; i++) {
                    score.next();
                    list.add(inGroup.getString(Const.STUDENTS_LASTNAME));
                    list.add(inGroup.getString(Const.STUDENTS_FIRSTNAME));
                    list.add(inGroup.getString(Const.STUDENTS_GROUP));
                    list.add(score.getString(Const.AVERAGESCORE_SCORE));

                }
            }
        }
        return list;
    }
}