module com.example.successmonitoring {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.desktop;


    opens com.example.successmonitoring to javafx.fxml;
    exports com.example.successmonitoring;
}