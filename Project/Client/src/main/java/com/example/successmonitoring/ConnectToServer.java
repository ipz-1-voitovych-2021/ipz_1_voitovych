package com.example.successmonitoring;

//import javax.swing.*;

import java.io.*;
import java.net.Socket;

public class ConnectToServer {
    static Socket socket;

    static {
        try {
            socket = new Socket("localhost", 3000);

        } catch (IOException e) {
            e.printStackTrace();
           // JOptionPane.showMessageDialog(null,"Немає з'єднання з сервером!");
        }
    }

    BufferedReader reader = new BufferedReader(
            new InputStreamReader(socket.getInputStream()));


    PrintWriter writer = new PrintWriter(
            new BufferedWriter(
                    new OutputStreamWriter(
                            socket.getOutputStream())), true);


    public ConnectToServer() throws IOException {
    }


    public String authorize(String login, String pass) throws IOException {
        writer.println("authorize");
        writer.println(login);
        writer.println(pass);

        return String.valueOf(reader.readLine());
    }
    public void addData(String lastName, String firstName, String middleName, String group, String year,
                        String term, String score) {
        writer.println("addData");
        writer.println(lastName);
        writer.println(firstName);
        writer.println(middleName);
        writer.println(group);
        writer.println(year);
        writer.println(term);
        writer.println(score);
    }
    public String searchRatingData(String param, String group) throws IOException {
        writer.println("searchRating");
        writer.println(param);
        writer.println(group);
        String search = String.valueOf(reader.readLine());
        if(search.equals("null")){
            return "null";
        }
        return search;
    }

    void closeClientAndServerTimerInitialization(String city){
        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        if(city.equals("")) {
                            javafx.application.Platform.exit();
                        }
                    }
                },
                3600000
        );
    }

    void closeServerIfClientIsInactive(String cityBefore,String city){
        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        if(city.equals(cityBefore)) {
                            javafx.application.Platform.exit();
                        }
                    }
                },
                3600000
        );
    }
}


