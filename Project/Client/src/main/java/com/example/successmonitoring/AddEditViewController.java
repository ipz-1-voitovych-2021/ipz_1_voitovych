package com.example.successmonitoring;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class AddEditViewController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button WorkWdbDeleteButton;

    @FXML
    private TextField WorkWdbEverageScoreField;

    @FXML
    private TextField WorkWdbFirstNameField;

    @FXML
    private Button WorkWdbGetBackButton;

    @FXML
    private TextField WorkWdbGroupField;

    @FXML
    private TextField WorkWdbLastNameField;

    @FXML
    private TextField WorkWdbMiddleNameField;

    @FXML
    private Button WorkWdbSaveButton;

    @FXML
    private TextField WorkWdbTermField;

    @FXML
    private TextField WorkWdbYearField;

    ConnectToServer connect = new ConnectToServer();

    public AddEditViewController() throws IOException {
    }

    @FXML
    void initialize() {
        WorkWdbGetBackButton.setOnAction(actionEvent -> {
            WorkWdbGetBackButton.getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("admin-menu.fxml"));
            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Parent root = loader.getRoot();
            Stage stage=new Stage();
            stage.setScene(new Scene(root));
            stage.show();
        });
        WorkWdbDeleteButton.setOnAction(actionEvent -> {
            String lastnameText = WorkWdbLastNameField.getText().trim();
            String firstnameText = WorkWdbFirstNameField.getText().trim();
            String middlenameText = WorkWdbMiddleNameField.getText().trim();
            String groupText = WorkWdbGroupField.getText().trim();
            String yearText = WorkWdbYearField.getText().trim();
            String termText = WorkWdbTermField.getText().trim();
            String scoreText = WorkWdbEverageScoreField.getText().trim();
            if (!lastnameText.equals("") && !firstnameText.equals("") &&
                    !middlenameText.equals("") && !groupText.equals("") &&
                    !yearText.equals("") && !termText.equals("")
                    && !scoreText.equals(""))
                deleteData();
            else
                System.out.println("One(some) field(s) is(are) empty");
            WorkWdbDeleteButton.getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("admin-menu.fxml"));
            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Parent root = loader.getRoot();
            Stage stage=new Stage();
            stage.setScene(new Scene(root));
            stage.show();
        });
    }
    public void saveData() throws IOException {
        connect.addData(WorkWdbLastNameField.getText(),WorkWdbFirstNameField.getText(),
                WorkWdbMiddleNameField.getText(),WorkWdbGroupField.getText(),
                WorkWdbYearField.getText(),WorkWdbTermField.getText(),WorkWdbEverageScoreField.getText());
        WorkWdbSaveButton.getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("admin-menu.fxml"));
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Parent root = loader.getRoot();
        Stage stage=new Stage();
        stage.setScene(new Scene(root));
        stage.show();
    }
    private void deleteData() {
    }
}


