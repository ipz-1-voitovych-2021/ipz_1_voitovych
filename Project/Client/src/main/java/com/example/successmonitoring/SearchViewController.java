package com.example.successmonitoring;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.stage.Stage;

public class SearchViewController {
    ObservableList<String> SearchViewGroupList = FXCollections.observableArrayList("1","4");
    ObservableList<String> SearchViewParamList = FXCollections.observableArrayList("у групі","серед всіх");


    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button SearchViewSearchButton;

    @FXML
    private Button SearchViewGetBackButton;

    @FXML
    private ChoiceBox SearchViewParamBox;

    @FXML
    private ChoiceBox SearchViewGroupBox;

    @FXML
    void initialize() {
        SearchViewParamBox.setValue("у групі");
        SearchViewParamBox.setItems(SearchViewParamList);

        SearchViewGroupBox.setValue("1");
        SearchViewGroupBox.setItems(SearchViewGroupList);

        SearchViewGetBackButton.setOnAction(actionEvent ->{
            SearchViewGetBackButton.getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("main-menu.fxml"));
            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Parent root = loader.getRoot();
            Stage stage=new Stage();
            stage.setScene(new Scene(root));
            stage.show();
        });
    }

}
