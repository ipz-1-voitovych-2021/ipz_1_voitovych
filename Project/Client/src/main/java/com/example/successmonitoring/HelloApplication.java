package com.example.successmonitoring;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class HelloApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("main-menu.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 700, 400);
        stage.setTitle("Success Monitoring App");
        stage.setScene(scene);
        stage.show();
        stage.setOnCloseRequest(event ->{javafx.application.Platform.exit();});
    }

    public static void main(String[] args) {Application.launch();}
}