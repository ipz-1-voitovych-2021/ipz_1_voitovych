package com.example.successmonitoring;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Observable;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import javax.swing.table.TableColumn;

public class RatingViewController {
    ObservableList<String> RatingViewGroupList = FXCollections.observableArrayList("1","4");
    ObservableList<String> RatingViewParamList = FXCollections.observableArrayList("у групі","серед всіх");

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button RatingViewSearchButton;

    @FXML
    private Button RatingViewGetBackButton;

    @FXML
    private ChoiceBox RatingViewGroupBox;

    @FXML
    private ChoiceBox RatingViewParamBox;

    @FXML
    private TableColumn firstNameColumn;

    @FXML
    private TableColumn groupCplumn;

    @FXML
    private TableColumn lastNameColumn;

    @FXML
    private TableColumn scoreColumn;

    ConnectToServer connect = new ConnectToServer();

    public RatingViewController() throws IOException {
    }

    @FXML
    void initialize() {
        RatingViewParamBox.setValue("у групі");
        RatingViewParamBox.setItems(RatingViewParamList);

        RatingViewGroupBox.setValue("1");
        RatingViewGroupBox.setItems(RatingViewGroupList);

        RatingViewGetBackButton.setOnAction(actionEvent ->{
            RatingViewGetBackButton.getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("main-menu.fxml"));
            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Parent root = loader.getRoot();
            Stage stage=new Stage();
            stage.setScene(new Scene(root));
            stage.show();
        });
    }
    ArrayList<String> searchResult() throws IOException {
        connect.searchRatingData(RatingViewParamBox.getValue().toString(),
                RatingViewGroupBox.getValue().toString());

        String[] res = connect.searchRatingData(RatingViewParamBox.getValue().toString(),
                RatingViewGroupBox.getValue().toString()).split(",");
        return new ArrayList<String>(Arrays.asList(res));
    }

    public void search() throws IOException {
        InfoBox info = new InfoBox();
        if(searchResult().get(0).equals("null")){
            info.noInfo();
        }

    }
}