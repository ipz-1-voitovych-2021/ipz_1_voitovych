package com.example.successmonitoring;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class SuccessTrackingViewController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField SuccessTrackingViewFirstNameField;

    @FXML
    private Button SuccessTrackingViewGetBackButton;

    @FXML
    private TextField SuccessTrackingViewLastNameField;

    @FXML
    private Button SuccessTrackingViewSearchButton;

    @FXML
    void initialize() {
        SuccessTrackingViewGetBackButton.setOnAction(actionEvent ->{
            String lastnameText = SuccessTrackingViewLastNameField.getText().trim();
            String firstnameText = SuccessTrackingViewFirstNameField.getText().trim();
            if (!lastnameText.equals("") && !firstnameText.equals(""))
                searchdata(lastnameText, firstnameText);
            else
                System.out.println("LastName and/or FirstName field(s) is empty");
            SuccessTrackingViewGetBackButton.getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("main-menu.fxml"));
            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Parent root = loader.getRoot();
            Stage stage=new Stage();
            stage.setScene(new Scene(root));
            stage.show();
        });
    }
    private void searchdata(String lastnameText, String firstnameText) {
    }
}
