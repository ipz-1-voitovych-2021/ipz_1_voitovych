package com.example.successmonitoring;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class LogInController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button EnterButton;

    @FXML
    private Button LogInGetBackButton;

    @FXML
    private TextField LoginField;

    @FXML
    private PasswordField PasswordField;

    ConnectToServer connect = new ConnectToServer();

    public LogInController() throws IOException {
    }

    @FXML
    void initialize() {
        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        if ((PasswordField.getText().isEmpty() && LoginField.getText().isEmpty())
                                || PasswordField.getText().isEmpty() || LoginField.getText().isEmpty()) {
                            javafx.application.Platform.exit();
                        }
                    }
                },
                3600000
        );
    }
    public void getBack() throws IOException{
        LogInGetBackButton.setOnAction(actionEvent ->{
            LogInGetBackButton.getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("main-menu.fxml"));
            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Parent root = loader.getRoot();
            Stage stage=new Stage();
            stage.setScene(new Scene(root));
            stage.show();
        });
    }
    public void loginAdmin() throws IOException {
        if (connect.authorize(LoginField.getText(),PasswordField.getText()).equals("admin")){
            EnterButton.getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("admin-menu.fxml"));
            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Parent root = loader.getRoot();
            Stage stage=new Stage();
            stage.setScene(new Scene(root));
            stage.show();
        }
        else {
            InfoBox info = new InfoBox();
            info.incorrectAdmin();
        }
    }
}


